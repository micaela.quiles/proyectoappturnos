import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:app_quilesvillarroel_turnos/Paginas/horaTurno_4.dart';
import 'package:app_quilesvillarroel_turnos/Paginas/registrarse.dart';

//stateful es para cuando el usuario va a interactuar con esa pantalla
class Directivos extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<Directivos> {
  //opciones del menú desplegable
  String valueChoose;
  List listitem = [
    'Director',
    'Preceptor',
    'Secretaria',
    'Asesoria',
    'Regencia'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.orange,
          title: new Center(
            child: new Text('Datos del turno',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 40, color: Colors.white))),
        ),
        body: Center(
          child: Column(
            children: <Widget>[
            SizedBox(height: 60), //agrega un espacio
            //tabla
            Expanded(
              child: ListView(
              children: [
              Container(
              margin: EdgeInsets.all(10), //ancho
              padding: EdgeInsets.all(20), //alto
              child: Table(
                border: TableBorder.all(),
                children: [
                  TableRow(children: [
                    Column(children: [
                      SizedBox(height: 40), //agrega un espacio
                      Text('Director', style: TextStyle(fontSize: 20))
                    ]),
                    Column(children: [
                      Text('Entrevistas o charlas más formales, para charlas sobre algún problema que haya ocurrido recientemente, o alguna inquietud.',
                      style: TextStyle(fontSize: 15))
                    ]),
                  ]),
                  TableRow(children: [
                    Column(children: [
                      SizedBox(height: 20), //agrega un espacio
                      Text('Preceptor', style: TextStyle(fontSize: 20))
                    ]),
                    Column(children: [
                      Text('Constancia de alumno regular, Informe o una charla acerca del alumno/a.',
                      style: TextStyle(fontSize: 15))
                    ]),
                  ]),
                  TableRow(children: [
                    Column(children: [
                      SizedBox(height: 20), //agrega un espacio
                      Text('Secretaria', style: TextStyle(fontSize: 20))
                    ]),
                    Column(children: [
                      Text('En caso de requerir toda la documentación e información del alumno/a.',
                      style: TextStyle(fontSize: 15))
                    ]),
                  ]),
                  TableRow(children: [
                    Column(children: [
                      SizedBox(height: 30), //agrega un espacio
                      Text('Asesoría', style: TextStyle(fontSize: 20))
                    ]),
                    Column(children: [
                      Text('Entrevistas para charlas sobre un tema en específico o en caso de necesitar un asesoramiento especial.',
                      style: TextStyle(fontSize: 15))
                    ]),
                  ]),
                  TableRow(children: [
                    Column(children: [
                      SizedBox(height: 20), //agrega un espacio
                      Text('Regencia', style: TextStyle(fontSize: 20))
                    ]),
                    Column(children: [
                      Text('Control de la documentación, asesoramiento de padres y alumnos/as.',
                      style: TextStyle(fontSize: 15))
                    ]),
                  ]),
                ],
              ),
            ),
            Container(
              child: Column(children: [
                SizedBox(height: 60),
                Align(
                    alignment: Alignment.center,
                    child: Text('¿Con quién desea solicitar un turno?',
                    style: TextStyle(fontSize: 20))),
                SizedBox(height: 40), //agrega un espacio
                //menú desplegable
                Container(
                  child: DropdownButton(
                      hint: Text("seleccionar una opción:   "),
                      value: valueChoose,
                      onChanged: (newValue) {//cuando cambia el valor del elemento
                        setState(() {
                          valueChoose = newValue;
                        });
                      },
                      items: listitem.map((valueItem) {
                        return DropdownMenuItem(
                          value: valueItem,
                          child: Text(valueItem),
                        );
                      }).toList()),
                ),
              ]),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: 50),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                  child: TextField(
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Escriba por que quiere el turno.',
                    ),
                  ),
                ),
              ],
            ),
            Row(
              children: [
                SizedBox(height: 200, width: 30), //agrega un espacio
                Expanded(// expande el elemento y al maximo que pueda
                  child: ElevatedButton(
                    onPressed: () {//cuando el usuario presiona el botón
                      Navigator.of(context).push(MaterialPageRoute(//dirección de la pantalla
                        builder: (context) => Login(), //a que pantalla va
                      ));
                    },
                    //estilo del botón
                    style: ElevatedButton.styleFrom(
                      primary: Colors.deepOrangeAccent),
                    child: Text("volver", style: TextStyle(fontSize: 25)),
                  ),
                ),
                SizedBox(width: 50), //agrega un espacio
                Expanded(
                  child: ElevatedButton(
                    onPressed: () {//cuando el usuario presiona el botón
                      Navigator.of(context).push(MaterialPageRoute(//dirección de la pantalla
                        builder: (context) => GetCheckValue(), //a que pantalla va
                      ));
                    },
                    //estilo del botón
                    style: ElevatedButton.styleFrom(
                      primary: Colors.deepOrangeAccent),
                    child: Text("siguiente", style: TextStyle(fontSize: 25)),
                  ),
                ),
                SizedBox(width: 30), //agrega un espacio
              ],
            )
          ],
        )),
      ])),
    );
  }
}
