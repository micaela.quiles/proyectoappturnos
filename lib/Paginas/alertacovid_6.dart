import 'package:flutter/material.dart';
import 'package:app_quilesvillarroel_turnos/Paginas/muchasGracias_7.dart';

//stateful es para cuando el usuario va a interactuar con esa pantalla
class AlertaCovid extends StatefulWidget {
  AlertaCovid({Key key}) : super(key: key);

  @override
  Alertacovid createState() => Alertacovid();
}

class Alertacovid extends State<AlertaCovid> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(//appbar centrado de color naranja
      appBar: AppBar(
        backgroundColor: Colors.orange,
        title: new Center(
          child: new Text('Alerta Covid-19',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 40, color: Colors.white)))),
      body: Column(
        children: [
          SizedBox(height: 50), //agrega un espacio
          Column(
            children: [
              Align(
                alignment: Alignment.center,
                child: Text('''  Si presenta algunos de estos sintomas:
                - fiebre
                - tos
                - dolor de garganta
                - perdida de olfato y/o gusto
                - malestar general
                Si ha tenido contacto estrecho con una persona que dió positivo en las ultimas 72hs.''',
                style: TextStyle(fontSize: 22))
              ),
          ],),
          Column(
            children: [
              SizedBox(height: 50), //agrega un espacio
              Align(
                alignment: Alignment.topLeft,
                child: Text(' NO asistir al turno.',
                style: TextStyle(fontSize: 24))
              ),
          ],),
          Column(
            children: [
              SizedBox(height: 20), //agrega un espacio
              Align(
                alignment: Alignment.topCenter,
                child: Text('En caso contrario lo dicho anteriormente: obligatorio el uso de barbijo y respetar la distancia social.',
                style: TextStyle(fontSize: 22))
              ),
          ],),
          Row(
            children: [
              SizedBox(height: 150, width: 200), //agrega un espacio
              Expanded(
                child: ElevatedButton(
                    onPressed: () { //cuando el usuario presiona el botón
                      Navigator.of(context).push(MaterialPageRoute(//dirección de la pantalla
                        builder: (context) => MuchasGracias(),//a que pantalla va
                      ));
                    },
                    //estilo del botón
                    style: ElevatedButton.styleFrom(
                      primary: Colors.deepOrangeAccent),
                    child: Text("siguiente",
                    style: TextStyle(fontSize: 25, color: Colors.white))),
              ),
              SizedBox(width: 30), //agrega un espacio
            ],
          )
        ],
      ),
    );
  }
}
