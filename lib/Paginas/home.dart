import 'package:flutter/material.dart';
import 'package:app_quilesvillarroel_turnos/Paginas/registrarse.dart';
import 'package:app_quilesvillarroel_turnos/Paginas/iniciarSesion.dart';

class Registrarse extends StatefulWidget {
  Registrarse({Key key}) : super(key: key);

  @override
  Registro createState() => Registro();
}

class Registro extends State<Registrarse> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: Column(
            children: [
              SizedBox(height: 170), //agrega un espacio
              Column(
                children: [
                  Align(
                    alignment: Alignment.topCenter,
                    child: Text('Bienvenido!',
                    style: TextStyle(fontSize: 40))
                  ),
              ],),
              Row(
                children: [
                  SizedBox(height: 250, width: 50), //agrega un espacio
                  Expanded(// expande el elemento y al maximo que pueda
                    child: ElevatedButton(
                      onPressed: () {//cuando el usuario presiona el botón
                        Navigator.of(context).push(MaterialPageRoute(//dirección de la pantalla
                          builder: (context) =>Login(), //a que pantalla va
                        ));
                      },
                      //estilo del botón
                      style: ElevatedButton.styleFrom(
                        primary: Colors.deepOrangeAccent),
                      child: Text("Registrarse",
                      style: TextStyle(fontSize: 20)),
                    ),
                  ),
                  SizedBox(width: 80), //agrega un espacio
                  Expanded(
                    child: ElevatedButton(
                      onPressed: () {//cuando el usuario presiona el botón
                        Navigator.of(context).push(MaterialPageRoute(//dirección de la pantalla
                          builder: (context) => Sesion(), //a que pantalla va
                        ));
                      },
                      //estilo del botón
                      style: ElevatedButton.styleFrom(
                        primary: Colors.deepOrangeAccent),
                      child: Text("Iniciar sesión",
                      style: TextStyle(fontSize: 20)),
                    ),
                  ),
                  SizedBox(width: 50), //agrega un espacio
                ],
              )
          ]),
        ),
      )
    );
  }
}
