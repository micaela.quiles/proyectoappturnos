import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:app_quilesvillarroel_turnos/Paginas/directivos_2.dart';

class Login extends StatelessWidget with InputValidationMixin {
  final formGlobalKey = GlobalKey<FormState>();

  String _email;
  String _password;

  @override
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormState>();

    return Scaffold(
      //appbar centrado de color naranja
      appBar: AppBar(
        backgroundColor: Colors.orange,
        title: new Center(
          child: new Text('Registrarse',
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 40, color: Colors.white))),
      ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30),
          child: Form(
            key: formGlobalKey, //identificar elementos de forma única
            child: ListView(
              children: [
                SizedBox(height: 60), //agrega un espacio
                TextFormField(
                  decoration: InputDecoration(labelText: "Nombre"),
                ),
                SizedBox(height: 25), //agrega un espacio
                TextFormField(
                  decoration: InputDecoration(labelText: "Apellido"),
                ),
                SizedBox(height: 25),
                TextFormField(
                  decoration: InputDecoration(labelText: "Número de teléfono"),
                ),
                SizedBox(height: 25),
                TextFormField(
                  decoration: InputDecoration(labelText: "DNI"),
                ),
                SizedBox(height: 25),
                TextFormField(
                  decoration: InputDecoration(labelText: "Nombre completo del alumno"),
                ),
                SizedBox(height: 25),
                TextFormField(
                  decoration: InputDecoration(labelText: "Parentesco con el alumno"),
                ),
                SizedBox(height: 25), //agrega un espacio
                TextFormField(
                  validator: (valor) => !valor.contains('@')?'Email Invalido': null,
                  onSaved: (valor) => _email = valor,
                  decoration: InputDecoration(labelText: 'Email'),
                ),
                SizedBox(height: 20.0),
                TextFormField(
                  validator: (valor) => valor.length < 8 ?'La contraseña es muy corta': null,
                  onSaved: (valor) => _password = valor,
                  obscureText: true,
                  decoration: InputDecoration(labelText: 'Contraseña'),
                ),
                Row(
                  children: [
                    SizedBox(height: 200, width: 180), //agrega un espacio
                    Expanded(
                      key: _formKey,
                      child: ElevatedButton(
                        onPressed: () { //cuando el usuario presiona el botón
                          Navigator.of(context).push(MaterialPageRoute(//dirección de la pantalla
                            builder: (context) => Directivos(), //a que pantalla va
                          ));
                        },
                        //estilo del botón
                        style: ElevatedButton.styleFrom(
                          primary: Colors.deepOrangeAccent),
                        child: Text("siguiente",
                        style: TextStyle(color: Colors.white, fontSize: 25)),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
      ));
  }
}

//validación de la contraseña y del email
mixin InputValidationMixin {
  bool isPasswordValid(String password) => password.length < 8;

  bool isEmailValid(String email) {
    Pattern pattern =
        '^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))';
    RegExp regex = new RegExp(pattern);
    return regex.hasMatch(email);
  }
}
