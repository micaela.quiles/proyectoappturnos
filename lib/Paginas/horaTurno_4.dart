import 'package:flutter/material.dart';
import 'package:app_quilesvillarroel_turnos/Paginas/directivos_2.dart';
import 'package:app_quilesvillarroel_turnos/Paginas/comprobante_5.dart';

//stateful es para cuando el usuario va a interactuar con esa pantalla
class GetCheckValue extends StatefulWidget {
  GetCheckValue({Key key}) : super(key: key);

  @override
  GetCheckValueState createState() {
    return new GetCheckValueState();
  }
}

class GetCheckValueState extends State<GetCheckValue> {
  //se definen las variables de las casillas de selección
  bool _isChecked = true;
  String _currText = '';

  List<String> text = ["9:00hs a 12:00hs", "14:00hs a 17:00hs"];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: ListView(
            children: [
              SizedBox(height: 100), //agrega un espacio
              Column(
                children: [
                  Align(
                    alignment: Alignment.topCenter,
                    child: Text("   Indique a que horario le resulta más cómodo...",
                    style: TextStyle(fontSize: 30))
                  ),
              ],),
              Column(
                children: [
                  SizedBox(height: 50), //agrega un espacio
                  Align(
                    alignment: Alignment.topCenter,
                    child: Text("(elija una sola opción)",
                    style: TextStyle(fontSize: 20)),
                  ),
              ],),
              /*
              Column(
                children: [
                  SizedBox(height: 50),//agrega un espacio
                  Align(
                    alignment: Alignment.topCenter,
                    child: Text("9hs a 13hs",
                    style: TextStyle(fontSize: 25)),
                  ),
              ],),
              Column(
                children: [
                  SizedBox(height: 50),
                  Align(
                    alignment: Alignment.topCenter,
                    child: Text("14hs a 17hs",
                    style: TextStyle(fontSize: 25)),
                  ),
              ],),
              */
              Expanded(
                  child: Container(
                child: Column(
                  children: text.map((t) => CheckboxListTile(
                    title: Text(t),
                    value: _isChecked,
                    onChanged: (val) {//cuando cambia el valor del elemento
                      setState(() {
                        _isChecked = val;
                        if (val == true) {
                          _currText = t;
                        }
                      });
                    }))
                  .toList(),
                ),
              )),
              Row(
                children: [
                  SizedBox(height: 270, width: 30), //agrega un espacio
                  Expanded(// expande el elemento y al maximo que pueda
                    child: ElevatedButton(
                      onPressed: () {//cuando el usuario presiona el botón
                        Navigator.of(context).push(MaterialPageRoute(//dirección de la pantalla
                          builder: (context) =>Directivos(), //a que pantalla va
                        ));
                      },
                      //estilo del botón
                      style: ElevatedButton.styleFrom(
                        primary: Colors.deepOrangeAccent),
                      child: Text("volver",
                      style: TextStyle(fontSize: 25)),
                    ),
                  ),
                  SizedBox(width: 50), //agrega un espacio
                  Expanded(
                    child: ElevatedButton(
                      onPressed: () {//cuando el usuario presiona el botón
                        Navigator.of(context).push(MaterialPageRoute(//dirección de la pantalla
                          builder: (context) => TurnoDado(), //a que pantalla va
                        ));
                      },
                      //estilo del botón
                      style: ElevatedButton.styleFrom(
                        primary: Colors.deepOrangeAccent),
                      child: Text("siguiente",
                      style: TextStyle(fontSize: 25)),
                    ),
                  ),
                  SizedBox(width: 30), //agrega un espacio
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
