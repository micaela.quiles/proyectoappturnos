import 'package:flutter/material.dart';
import 'dart:async';
import 'package:app_quilesvillarroel_turnos/Paginas/home.dart';

//stateless es para el usuaio no interactua con la pantalla
class Splash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Splash Screen',
      theme: ThemeData(
        primarySwatch: Colors.orange, //primarySwatch no es un color. Es MaterialColor. Lo que significa que son los diferentes tonos de color que usará una aplicación de material.
      ),
      home: MyHomePage(),
      debugShowCheckedModeBanner: false, //Activa un pequeño banner "DEBUG" en modo marcado para indicar que la aplicación está en modo marcado.
    );
  }
}

//stateful es para cuando el usuario va a interactuar con esa pantalla
class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    super.initState(); //se llama cuando el objeto se inserta en el árbol
    Timer(
      Duration(seconds: 3), //tiempo de duración del splash
      () => Navigator.pushReplacement(context,MaterialPageRoute(//dirección de la pantalla
      builder: (context) => Registrarse()))
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      //degradado
      decoration: BoxDecoration(
        //image: DecorationImage(image: AssetImage('assets/icon/LogoApp.png')),
        gradient: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [
          Colors.orange,
          Colors.lightBlue,
        ],
      )),
    );
  }
}
