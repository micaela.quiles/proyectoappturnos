import 'package:flutter/material.dart';
import 'package:app_quilesvillarroel_turnos/Paginas/alertacovid_6.dart';
import 'package:app_quilesvillarroel_turnos/Paginas/registrarse.dart';

//stateful es para cuando el usuario va a interactuar con esa pantalla
class TurnoDado extends StatefulWidget {
  TurnoDado({Key key}) : super(key: key);

  @override
  Turnosdados createState() => Turnosdados();
}

class Turnosdados extends State<TurnoDado> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.orange,
          title: new Center(
            child: new Text('Comprobante de turno',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 40, color: Colors.white))),
        ),
        body: Center(
        child: ListView(
          children: [
            SizedBox(height: 50), //agrega un espacio
            Column(
              children: [
                SizedBox(height: 30), //agrega un espacio
                Align(
                  alignment: Alignment.topLeft,
                  child: Text('  Nombre: ', style: TextStyle(fontSize: 25))
                ),
            ],),
            Column(
              children: [
                SizedBox(height: 10), //agrega un espacio
                Align(
                  alignment: Alignment.topLeft,
                  child: Text('  Apellido: ',
                  style: TextStyle(fontSize: 25))
                ),
            ],),
            Column(
              children: [
                SizedBox(height: 10), //agrega un espacio
                Align(
                  alignment: Alignment.topLeft,
                  child: Text('  DNI: ',
                  style: TextStyle(fontSize: 25))
                ),
            ],),
            Column(
              children: [
                SizedBox(height: 10), //agrega un espacio
                Align(
                  alignment: Alignment.topLeft,
                  child: Text('  Número de teléfono: ',
                  style: TextStyle(fontSize: 25))
                ),
            ],),
            Column(
              children: [
                SizedBox(height: 10), //agrega un espacio
                Align(
                  alignment: Alignment.topLeft,
                  child: Text('  Parentesco con el alumno: ',
                  style: TextStyle(fontSize: 25))
                ),
            ],),
            Column(
              children: [
                SizedBox(height: 10), //agrega un espacio
                Align(
                  alignment: Alignment.topLeft,
                  child: Text('  Nombre completo del alumno: ',
                  style: TextStyle(fontSize: 25))
                ),
            ],),
            Column(
              children: [
                SizedBox(height: 50), //agrega un espacio
                Align(
                  alignment: Alignment.topLeft,
                  child: Text('''  Su turno es el día: .../.../...
                  a las ...hs.''',
                  style: TextStyle(fontSize: 20))
                ),
            ],),
            Column(
              children: [
                SizedBox(height: 40), //agrega un espacio
                Align(
                  alignment: Alignment.topLeft,
                  child: Text('  En la dirección: Lanin 2020.',
                  style: TextStyle(fontSize: 20))
                ),
            ],),
            Column(
              children: [
                SizedBox(height: 30), //agrega un espacio
                Align(
                  alignment: Alignment.topLeft,
                  child: Text('Es necesario asistir al turno con su DNI en mano',
                  style: TextStyle(fontSize: 20))
                ),
            ],),
            Column(
              children: [
                SizedBox(height: 30), //agrega un espacio
                Align(
                  alignment: Alignment.topLeft,
                  child: Text('Es necesario que el guardia de la institución vea este comprobante.',
                  style: TextStyle(fontSize: 20))
                ),
            ],),
            Column(
              children: [
                SizedBox(height: 70), //agrega un espacio
                Align(
                  alignment: Alignment.topLeft,
                  child: Text('  En caso de no querer este turno pulse "cancelar".',
                   style: TextStyle(fontSize: 20))
                ),
            ],),
            Row(
              children: [
                SizedBox(height: 150, width: 30), //agrega un espacio
                Expanded(// expande el elemento y al maximo que pueda
                  child: ElevatedButton(
                    onPressed: () {//cuando el usuario presiona el botón
                      Navigator.of(context).push(MaterialPageRoute(//dirección de la pantalla
                        builder: (context) => Login(), //a que pantalla va
                      ));
                    },
                    //estilo del botón
                    style: ElevatedButton.styleFrom(
                      primary: Colors.deepOrangeAccent),
                    child: Text("cancelar", style: TextStyle(fontSize: 25)),
                  ),
                ),
                SizedBox(width: 50), //agrega un espacio
                Expanded(
                  child: ElevatedButton(
                    onPressed: () {//cuando el usuario presiona el botón
                      Navigator.of(context).push(MaterialPageRoute(//dirección de la pantalla
                        builder: (context) => AlertaCovid(), //a que pantalla va
                      ));
                    },
                    //estilo del botón
                    style: ElevatedButton.styleFrom(
                      primary: Colors.deepOrangeAccent),
                    child: Text("siguiente", style: TextStyle(fontSize: 25)),
                  ),
                ),
                SizedBox(width: 30), //agrega un espacio
              ],
            )
          ],
        ),
      )
    );
  }
}
