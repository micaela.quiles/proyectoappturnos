import 'package:flutter/material.dart';
import 'package:app_quilesvillarroel_turnos/Paginas/directivos_2.dart';

class Sesion extends StatefulWidget {
  Sesion({Key key}) : super(key: key);

  @override
  IniciarSesion createState() => IniciarSesion();
}

class IniciarSesion extends State<Sesion> {
  final formGlobalKey = GlobalKey<FormState>();
  final _formKey = GlobalKey<FormState>();
  bool _obscureText = true;

  String _email;
  String _password;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orange,
        title: new Center(child: new Text('Iniciar Sesión',
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 40, color: Colors.white))),
      ),
      body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30),
          child: Form(
            key: formGlobalKey, //identificar elementos de forma única
            child: ListView(
              children: [
                SizedBox(height: 60), //agrega un espacio
                TextFormField(
                  validator: (valor) => !valor.contains('@')?'Email Invalido': null,
                  onSaved: (valor) => _email = valor,
                  decoration: InputDecoration(labelText: 'Email'),
                ),
                SizedBox(height: 20.0),
                TextFormField(
                  validator: (valor) => valor.length < 8 ?'La contraseña es muy corta': null,
                  onSaved: (valor) => _password = valor,
                  obscureText: _obscureText,
                  decoration: InputDecoration(labelText: 'Contraseña'),
                ),
                new FlatButton(onPressed: _toggle, child: Icon(Icons.remove_red_eye_sharp)),
                Row(
                  children: [
                    SizedBox(height: 200, width: 180), //agrega un espacio
                    Expanded(
                      key: _formKey,
                      child: ElevatedButton(
                        onPressed: () { //cuando el usuario presiona el botón
                          Navigator.of(context).push(MaterialPageRoute(//dirección de la pantalla
                            builder: (context) => Directivos(), //a que pantalla va
                          ));
                        },
                        //estilo del botón
                        style: ElevatedButton.styleFrom(
                          primary: Colors.deepOrangeAccent),
                        child: Text("siguiente",
                        style: TextStyle(color: Colors.white, fontSize: 25)),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
      )
    );
  }
}
