import 'package:flutter/material.dart';
import 'package:app_quilesvillarroel_turnos/Paginas/alertacovid_6.dart';
import 'package:app_quilesvillarroel_turnos/Paginas/home.dart';

//stateful es para cuando el usuario va a interactuar con esa pantalla
class MuchasGracias extends StatefulWidget {
  MuchasGracias({Key key}) : super(key: key);

  @override
  Muchasgracias createState() => Muchasgracias();
}

class Muchasgracias extends State<MuchasGracias> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: ListView(
          children: [
          SizedBox(height: 150), //agrega un espacio
          Column(
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: Text(' Su turno ha sido agendado correctamente.',
                style: TextStyle(fontSize: 30))
              ),
          ],),
          Column(
            children: [
              SizedBox(height: 50), //agrega un espacio
              Align(
                alignment: Alignment.center,
                child: Text('Los esperamos',
                style: TextStyle(fontSize: 25))
              ),
          ],),
          Column(
            children: [
              SizedBox(height: 30), //agrega un espacio
              Align(
                alignment: Alignment.center,
                child: Text('Muchas gracias!',
                style: TextStyle(fontSize: 25))
              ),
          ],),
          Row(
            children: [
              SizedBox(height: 250, width: 30), //agrega un espacio
              Expanded(// expande el elemento y al maximo que pueda
                child: ElevatedButton(
                  onPressed: () {//cuando el usuario presiona el botón
                    Navigator.of(context).push(MaterialPageRoute(//dirección de la pantalla
                      builder: (context) => AlertaCovid(), //a que pantalla va
                    ));
                  },
                  //estilo del botón
                  style: ElevatedButton.styleFrom(
                      primary: Colors.deepOrangeAccent),
                  child: Text("volver", style: TextStyle(fontSize: 25)),
                ),
              ),
              SizedBox(width: 50), //agrega un espacio
              Expanded(
                child: ElevatedButton(
                  onPressed: () {//cuando el usuario presiona el botón
                    Navigator.of(context).push(MaterialPageRoute(//dirección de la pantalla
                      builder: (context) => Registrarse(),//a que pantalla va
                    ));
                  },
                  //estilo del botón
                  style: ElevatedButton.styleFrom(
                    primary: Colors.deepOrangeAccent),
                  child: Text("Finalizar", style: TextStyle(fontSize: 25)),
                ),
              ),
              SizedBox(width: 30),
            ],
          )
        ],
      ))),
    );
  }
}
