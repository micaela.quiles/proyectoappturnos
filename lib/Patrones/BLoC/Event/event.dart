import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_quilesvillarroel_turnos/Patrones/BLoC/Login/logicalogin.dart';

class HomesEvent {}

class LoadedEvent extends HomesEvent {}

class NotLoadedEvent extends HomesEvent {
  final int position;

  NotLoadedEvent(this.position);

}

//Creando el patrón BLoC
//Pantalla de login
class Login {}

class LoginAceptado extends Login {} //este quedaría así

class LoginRechazado extends Login {}

class LoginError extends Login {}

class LoginDesloguear extends Login {}

class LoginCerrar extends Login {}

class LoginSuspendido extends Login {}

class LoginBloqueado extends Login {}

//pantalla para elegir con quien quiere el turno
class Eligiendo {}

class EligiendoDirector extends Eligiendo {} //queda así

class EligiendoPreceptor extends Eligiendo {} //queda así

class EligiendoSecretaria extends Eligiendo {} //queda así

class EligiendoAsesoria extends Eligiendo {} //queda así

class EligiendoRegencia extends Eligiendo {} //queda así

class EligiendoBlanco extends Eligiendo {}

//Pantalla para elegir el horario
class SacandoTurno {}

class SacandoTurnoSeleccionada extends SacandoTurno {} //este quedaría así

class SacandoTurnoBlanco extends SacandoTurno {}
