import 'dart:async';
import 'package:app_quilesvillarroel_turnos/Patrones/BLoC/State/state.dart';
import 'package:app_quilesvillarroel_turnos/Patrones/BLoC/Event/event.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomesBloc extends Bloc<HomesEvent, HomesState> {
  HomesBloc(HomesState initialState) : super(initialState) {
    add(LoadedEvent());
  }

  @override
  Stream<HomesState> mapEventToState(HomesEvent event) async* {
    if (event is LoadedEvent) {
      print('Cargando algo');
      yield this.state.copiarAlgo(loading: true, algoNuevo: []);
      await Future.delayed(Duration(seconds: 2));
    } else if (event is NotLoadedEvent) {
      print('No se pudo: ${event.position}');
    }
  }
}
