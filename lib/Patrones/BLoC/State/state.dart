import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class HomesState extends Equatable {
  final bool loading;
  final List<dynamic> algoNuevo;

  HomesState({required this.loading, required this.algoNuevo});

  static HomesState initialState() => HomesState(loading: false, algoNuevo: []);

  HomesState copiarAlgo({bool loading: true, List<dynamic> algoNuevo}) {
    return HomesState(
        loading: loading ?? this.loading,
        algoNuevo: algoNuevo ?? this.algoNuevo);
  }

  @override
  List<Object> get props => [this.loading, this.algoNuevo];
}
