import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_quilesvillarroel_turnos/Patrones/BLoC/bloc.dart';

class HomePage extends StatefulWidget {
  HomePage({required Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final HomesBloc _homesBloc = HomesBloc(bool, HomesBloc);

  @override
  void dispose() {
    super.dispose();
    _homesBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    print('Estoy en el home');
    return BlocProvider.value(
      value: _homesBloc,
      child: Scaffold(
        body: Container(
          width: double.infinity,
          height: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              BlocBuilder<bool, HomesBloc>(
                builder: (_, state) {
                  print('estoy construyendo el patrón');
                  return Text('Termine de hacer $state');
                  //falta poner el input
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
